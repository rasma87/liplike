 LiPLike
================

About LiPLike
-------------
LiPLike is a tool to extract gene regulatory network interactions that are uniquely defined in the data. This is done by independently removing each regulator of a system, and observing the difference in the residual sum of squares (RSS). LiPLike calculates the increase in RSS as a fraction of RSS of full network. In other words, if an interaction is irreplaceable, LiPLike returns a value q > 1, whereas q ~= 1 indicates that an interaction is not uniquely defined (or unnecessary) in data.
2019-05-29

Installation
============
The package can be used under the license GNU Affero General Public License V3

Python Version Supported/Tested
-------------------------------
- Python 3.6

Dependencies
------------
- [NumPy version >1.12.1](https://www.numpy.org/)

Optional:
- [scikit-learn version > 0.19.2](https://scikit-learn.org/stable/)


Usage:
======
In python:
```python
# Here, X is an (n,k) matrix, and Y is an (m,k) matrix of floats. 
>>> import LiPLike as lpl
>>> L = lpk.LiPLike()
>>> L.fit(X, Y)
>>> print(L.q)

# If the system is underdetermined, we can use an l2-norm
>>> L = lpk.LiPLike()
>>> L.fit(X, Y, penalty_term='l2')
>>> print(L.q)

# Or, we can use make LiPLike to calculate a prior matrix from a correlation cutoff
>>> L = lpk.LiPLike()
>>> L.fit(X, Y, use_cov=True)
>>> print(L.q)
```



Contributor:
=============

 Rasmus Magnusson : Development of the package.

Current Members in the Project
------------------------------
- @rasma87

References
============
- [Bioarchive- LiPLike: Towards gene regulatory network predictions of high-certainty](https://www.biorxiv.org/content/10.1101/651596v1)
